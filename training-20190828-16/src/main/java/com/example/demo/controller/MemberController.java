package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.dao.MemberRepository;
import com.example.demo.model.MemberAccountJPA;

@Controller
public class MemberController {

	
	
	
	@Autowired
	MemberRepository memberRepository;

	@Autowired
	DataSource datasource;

	@GetMapping("/addMemberPage")
	public String addMemberPage() {

		List<MemberAccountJPA> memberAccountJPA = new ArrayList<MemberAccountJPA>();
		memberAccountJPA = memberRepository.findAll();

		for (int i = 0; i < memberAccountJPA.size(); i++) {
			System.out.println(memberAccountJPA.get(i).getId());
		}
		return "addMemberPage";
	}

	@GetMapping("/login")
	public String login(@ModelAttribute MemberAccountJPA memberAccountJPA) {

		return "login";
	}

	@PostMapping("/doLogin")
	public String doLogin(@ModelAttribute MemberAccountJPA memberAccountJPA, HttpSession session) {
		String email = memberAccountJPA.getEmail();
		String password = memberAccountJPA.getPassword();
		List<MemberAccountJPA> MemberAccountJPAList = new ArrayList<MemberAccountJPA>();
		MemberAccountJPAList = memberRepository.findCheckMemberAccount(email, password);
		MemberAccountJPA memberAccount = new MemberAccountJPA();
		memberAccount.setPassword(password);
		memberAccount.setEmail(email);
		;
		session.setAttribute("uid", memberAccount); //將memberAccount object存入session命名為uid
		if (MemberAccountJPAList.size() == 0) {
			return "login";
		} else {
			session.setAttribute("uid", memberAccount);
			return "welcome";
		}

	}

}
