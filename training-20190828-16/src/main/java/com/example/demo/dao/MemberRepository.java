package com.example.demo.dao;

import com.example.demo.model.MemberAccountJPA;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;



public interface MemberRepository  extends JpaRepository<MemberAccountJPA, Long>{
	List<MemberAccountJPA> findAll();
	
	List<MemberAccountJPA> findByEmail(String email);
	
	@Query(value="select id,email,password,address,cellphone from memberaccountjpa where email = ?1 and password = ?2 " ,nativeQuery = true)
	List<MemberAccountJPA> findCheckMemberAccount(String email,String password);

}
