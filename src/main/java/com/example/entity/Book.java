package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_book")
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO) // 自動產生流水號
  @Column(name = "id")
  private Long id;

  @Column(name = "book_name") // 重新設定
  private String name;
  
  private Integer price;

  @Column(name = "publishing_house")
  private String publishe;

}
