package com.example.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.entity.Book;
import com.example.repository.BookRepository;

@Controller
@RequestMapping(path = "/book")
public class BookController {

	@Autowired
	private BookRepository bookRepository;

	// SSH 架構 是使用 全域變數 實作 SETTER/GETTER
//  public List<Book> books; // 實作 SETTER / GETTER

	/** 列表頁 **/
	@GetMapping(path = "/list")
	public ModelAndView list() {
		ModelAndView mv = new ModelAndView("bookfile/list");
		List<Book> books = bookRepository.findAll();
//    System.err.println(books);
		mv.addObject("books", books);
		return mv;
	}

	/** 新增頁 **/
	@GetMapping(path = "/add")
	public ModelAndView add() {
		return new ModelAndView("bookfile/add");
	}

	/** 修改頁 **/
	@GetMapping(path = "/edit/{id}")
	public ModelAndView edit(@PathVariable Long id) {
		ModelAndView mv = new ModelAndView("bookfile/edit");
		Book book = bookRepository.findById(id).orElse(null);
		mv.addObject("book", book);
		return mv;
	}

	/** 儲存行為 建議使用 **/
	@PostMapping(path = "/save")
	public ModelAndView save(Book book) {
//    System.err.println(book);
		book = bookRepository.save(book);
		ModelAndView modelAndView = new ModelAndView("redirect:/book/edit/" + book.getId());
		return new ModelAndView("redirect:/book/list");
	}

	/** 儲存行為 建議使用 **/
	@PostMapping(path = "/add")
	public ModelAndView add(Book book) {
//    System.err.println(book);
		ModelAndView mv = new ModelAndView("bookfile/add");
		book = bookRepository.save(book);
		ModelAndView modelAndView = new ModelAndView("redirect:/book/edit/" + book.getId());
		return mv;
	}

	/** 刪除行為 **/
	@ResponseBody
	@DeleteMapping(path = "/delete/{id}")
	public String delete(@PathVariable Long id) {
		bookRepository.deleteById(id);
		return "SUCCESS";
	}

}
